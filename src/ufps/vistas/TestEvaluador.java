/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.vistas;

import Evaluador.EvaluadorExpresiones;
import Evaluador.Expresion;

/**
 *
 * @author estudiante
 */
public class TestEvaluador {

    public static void main(String[] args) {
        String url = "https://gitlab.com/madarme/archivos-persistencia/raw/master/expresiones/expresion.txt.txt";
        Evaluador.EvaluadorExpresiones ev = new Evaluador.EvaluadorExpresiones(url);
        System.out.println("Expresiones infijas  : \n" + ev.toString());

        for (Expresion e : ev.getExpresiones()) {
            try {
                System.out.println("Expresion Prefija:  " + e.getPrefijo());
                System.out.println("Expresion Postfija:  " + e.getPostfija());
                System.out.println("Resulado:  " + e.getEvaluarPosfijo() + "\n\n");
            } catch (Exception err) {
                err.getMessage();
            }
        }
    }
}
