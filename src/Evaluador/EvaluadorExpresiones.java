package Evaluador;

import ufps.util.colecciones_seed.*;
import ufps.util.varios.ArchivoLeerURL;

public class EvaluadorExpresiones {

    private ListaCD<Expresion> expresiones = new ListaCD();

    public EvaluadorExpresiones(String url) {

        ArchivoLeerURL file = new ArchivoLeerURL(url);
        Object v[] = file.leerArchivo();
        for (Object dato : v) {
            Expresion nueva = new Expresion(dato.toString());
            this.expresiones.insertarAlFinal(nueva);
        }
    }

    @Override
    public String toString() {
        String msg = "";
        for (Expresion dato : this.expresiones) {
            msg += dato.toString() + "\n";
        }
        return msg;
    }

    public ListaCD<Expresion> getExpresiones() {
        return expresiones;
    }

    public void setExpresiones(ListaCD<Expresion> expresiones) {
        this.expresiones = expresiones;
    }
}
