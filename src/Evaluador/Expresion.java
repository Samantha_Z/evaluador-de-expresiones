package Evaluador;

import ufps.util.colecciones_seed.*;

public class Expresion {

    private ListaCD<String> expresiones = new ListaCD();

    public Expresion() {
    }

    public Expresion(String cadena) {

        String v[] = cadena.split(",");
        for (String dato : v) {
            this.expresiones.insertarAlFinal(dato);
        }
    }

    public ListaCD<String> getExpresiones() {
        return expresiones;
    }

    public void setExpresiones(ListaCD<String> expresiones) {
        this.expresiones = expresiones;
    }

    @Override
    public String toString() {
        String msg = "";
        for (String dato : this.expresiones) {
            msg += dato + "";
        }
        return msg;
    }


    public boolean validarExpresion() {
        return validarSignos() && validarParentesis();
    }

    private boolean esSigno(char c) {
        return c == '+' || c == '-' || c == '*' || c == '/' || c == '^';
    }

    private boolean validarParentesis() {

        String cadena = this.expresiones.toString();
        cadena.replaceAll("", "");

        Pila<Character> parentesis = new Pila<>();

        for (int i = 0; i < cadena.length(); i++) {
            if (!(cadena.charAt(i) >= '0' && cadena.charAt(i) <= '9')) {
                if (cadena.charAt(i) == '(') {
                    parentesis.apilar('(');
                    if (i > 0) {
                        if (!(esSigno(cadena.charAt(i - 1)) || cadena.charAt(i - 1) == '(')) {
                            return false;
                        }
                    }
                } else if (cadena.charAt(i) == ')') {
                    if (parentesis.esVacia()) {
                        return false;
                    } else if (!esSigno(cadena.charAt(i - 1))) {
                        parentesis.desapilar();
                    }
                }
            }
        }
        return parentesis.esVacia();
    }

    private boolean validarSignos() {

        String cadena = this.expresiones.toString();
        cadena.replaceAll("", "");
        
        boolean noSigno = true; // variable para validar que no hayan signos seguidos
        boolean noSigno2 = false; //variable para validar que no hayan dos ~ seguidos

        for (int i = 0; i < cadena.length(); i++) {
            if (!(cadena.charAt(i) >= '0' && cadena.charAt(i) <= '9')) {
                if (esSigno(cadena.charAt(i)) && !noSigno) {
                    noSigno = true;
                } else {
                    if (cadena.charAt(i) == '~') {
                        noSigno2 = true;
                    } else {
                        return false;
                    }
                }
            } else {
                noSigno = false;
                noSigno2 = false;
            }
        }
        return !noSigno;
    }

    /*
    * Metodo que convierte la notacion infija en prefija
     */
    public String getPrefijo() throws Exception {
        return getPrefijo1();
    }

    private String getPrefijo1() throws Exception {

        if (validarExpresion()) {
            throw new Exception("Expresión no valida");
        }

        String cadena = this.expresiones.toString();
        cadena=cadena.replaceAll("<->", "");

        String prefija = "";
        Pila<Character> aux = new Pila<>();
        char notacion[] = cadena.toCharArray();

        for (int i = notacion.length - 1; i >= 0; i--) {
            if (notacion[i] == ')') {
                aux.apilar(notacion[i]);
            } else {
                if (esSigno(notacion[i])) {
                    if (aux.esVacia()) {
                        aux.apilar(notacion[i]);
                        prefija += " ";
                    } else {
                        while (esMayor(notacion[i], aux)) {
                            prefija += aux.desapilar();
                        }
                        prefija += " ";
                        aux.apilar(notacion[i]);
                    }
                } else if (notacion[i] == '(') {
                    char c = aux.desapilar();
                    while (c != ')') {
                        prefija += " " + c;
                        c = aux.desapilar();
                    }
                } else {
                    if (notacion[i] == '~') {
                        prefija += "-";
                    } else {
                        prefija += notacion[i];
                    }
                }
            }
        }
        while (!aux.esVacia()) {
            prefija += " " + aux.desapilar();
        }
        String prefij = "";
        for (int i = prefija.length() - 1; i >= 0; i--) {
            prefij += prefija.charAt(i);
        }
        return prefij;
    }

    /**
     * Metodo que  verifica si el tope de la pila es mayor a c caracter sobre
     * el cual voy a realizar la consulta , Pila (p) de
     */
    private boolean esMayor(char c, Pila<Character> p) {
        if (p.esVacia()) {
            return false;
        }
        char tope = p.desapilar();
        p.apilar(tope);
        if (c == '+' || tope == '-') {
            if (tope == ')') {
                return false;
            } else if (tope != '+' && tope != '-') {
                return true;
            }
        } else if (c == '*' || c == '/') {
            return tope == '^';
        }
        return false;
    }

    /*
    *  Metodo que convierte la notacion infija en posfija
     */
    public String getPostfija() throws Exception {
        return getPosfija1();
    }

    private String getPosfija1() throws Exception {

        if (validarExpresion()) {
            throw new Exception("Expresión no valida");
        }
       
        String cadena = this.expresiones.toString();
         cadena=cadena.replaceAll("<->", "");

        String posfija = "";
        char notacion[] = cadena.toCharArray();
        Pila<Character> aux = new Pila<>();
        
        for (int i = 0; i < notacion.length; i++) {
            if (notacion[i] >= '0' && notacion[i] <= '9' || notacion[i] == '~') {
                if (notacion[i] == '~') {
                    posfija += "-";
                } else {
                    posfija += notacion[i];
                }
            } else {
                if (notacion[i] != ')') {
                    if (deboApilar(notacion[i], aux)) {
                        aux.apilar(notacion[i]);
                        if (notacion[i] != '(') {
                            posfija += " ";
                        }
                    } else {
                        char c;
                        while (!deboApilar(notacion[i], aux)) {
                            c = aux.desapilar();
                            posfija += " " + c;
                        }
                        aux.apilar(notacion[i]);
                        posfija += " ";
                    }
                } else {
                    char c = aux.desapilar();
                    while (c != '(') {
                        posfija += " " + c;
                        c = aux.desapilar();
                    }
                }
            }
        }
        while (!aux.esVacia()) {
            posfija += " " + aux.desapilar();
        }
        return posfija;
    }

    /*Metodo que valida si se debe apilar o no dependiendo de los signos
     * @param c caracter sobre el cual voy a realizar la consulta , Pila (p) de signos
     * @return true si se apila
     */
    private boolean deboApilar(char c, Pila<Character> p) {
      
        if (p.esVacia()) 
            return true;
        
        char tope = p.desapilar();
        p.apilar(tope);//se vuelve a apilar para no afectar el funcionamiento
        
        switch (c) {
            case '(':
                return true;
            case '+':
            case '-':
                return tope == '(';
            case '*':
            case '/':
                return tope == '(' || tope == '+' || tope == '-';
            case '^':
                return true;
            default:
                break;
        }
        return true;
    }
    
    /*
    *Metodo que evalua la expresion posfija
     */
    public String getEvaluarPosfijo() throws Exception {

        return getResultado();
    }

    public String getResultado() throws Exception {
        
        String cadena = this.expresiones.toString();
         cadena=cadena.replaceAll("<->", "");
        
        String pos = this.getPostfija();
        
        Pila<Integer> numeros = new Pila<>();
        String numTemp = "";
        boolean esNegativo = false;
        
        for (int i = 0; i < pos.length(); i++) {
            if (pos.charAt(i) == '-') {
                if (i + 1 < pos.length()) {
                    if (pos.charAt(i + 1) != ' ') {
                        esNegativo = true;
                    }
                }
            }
            if (pos.charAt(i) == '+' || (pos.charAt(i) == '-' && !esNegativo) || pos.charAt(i) == '*' || pos.charAt(i) == '/' || pos.charAt(i) == '^') {
                int operando2 = numeros.desapilar();
                int operando1 = numeros.desapilar();
                int result = 0;
                switch (pos.charAt(i)) {
                    case '+':
                        result = operando1 + operando2;
                        break;
                    case '-':
                        result = operando1 - operando2;
                        break;
                    case '*':
                        result = operando1 * operando2;
                        break;
                    case '/':
                        if (operando2 == 0) {
                            throw new Exception("Error Matematico");
                        } else {
                            result = operando1 / operando2;
                        }
                        break;
                    case '^':
                        result = (int) Math.pow(operando1, operando2);
                        break;
                    default:
                        break;
                }
                numeros.apilar(result);
            } else if (pos.charAt(i) != ' ') {
                numTemp += pos.charAt(i);
            } else {
                if (!"".equals(numTemp)) {
                    int numero = Integer.parseInt(numTemp);
                    numeros.apilar(numero);
                }
                numTemp = "";
                esNegativo = false;
            }
        }
        return numeros.desapilar().toString();
    }

    /*
    *Metodo que convierte una cadena a un vector 
     */
    private String[] convertirVector(String cadena) {
        
        String vector = "";
        String[] expresion = new String[cadena.length()];
        int aux = 0;
        
        for (int i = 0; i < cadena.length(); i++) {
            if (cadena.charAt(i) != '+' && cadena.charAt(i) != '-' && cadena.charAt(i) != '*' && cadena.charAt(i) != '/'
                    && cadena.charAt(i) != '8' && cadena.charAt(i) != ')') {
                vector += cadena.charAt(i);
            } else {
                if (!vector.equals("")) {
                    expresion[aux] = vector;
                    aux++;
                    vector = "";
                    expresion[aux] = String.valueOf(cadena.charAt(i));
                    aux++;
                }
            }
            if (!cadena.isEmpty()) {
                expresion[aux] = vector;
            }
        }
        return expresion;
    }

}
